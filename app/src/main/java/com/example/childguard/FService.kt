package com.example.childguard

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.IBinder
import java.util.*
import androidx.core.app.NotificationCompat
import android.graphics.Color
import android.os.Handler




class FService : Service() {
    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    companion object {
        var isRunning  = false
        var isFirst = true
    }

    override fun onDestroy() {
        super.onDestroy()
        isRunning = false
    }

    override fun onLowMemory() {
        super.onLowMemory()
        isRunning = false
    }

    override fun onCreate() {
        super.onCreate()
        val notification = NotificationCompat.Builder(this)
            .setSmallIcon(android.R.mipmap.sym_def_app_icon)
            .setContentTitle(resources.getString(R.string.app_name))
            .setContentText("I am watching You")
            .setChannelId(
                createNotificationChannel())
            .build()
        startForeground(137231, notification)
    }

    private fun createNotificationChannel(): String{
        val channelId = "my_service"
        val channelName = "My Background Service"
        val chan = NotificationChannel(channelId,
            channelName, NotificationManager.IMPORTANCE_LOW)
        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE

        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(chan)
        return channelId
    }
private fun checkIsInForeground() {
    Handler().postDelayed({
        if(!(applicationContext as App).inForegroud && (applicationContext as App).holdInApp) {
            startActivity(Intent(applicationContext, MainActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                addCategory("android.intent.category.LAUNCHER")
            })
        }
        checkIsInForeground()
    }, 1000)
}
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
            isRunning = true
     checkIsInForeground()
        val intent = Intent(applicationContext, HourBroadcastReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(applicationContext, 23432, intent, 0)
        val alarmManager =  getSystemService(ALARM_SERVICE) as AlarmManager
        alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 500
            , pendingIntent)

            val handleServiceStop = object : HourBroadcastReceiver.HandleServiceStop {
                override fun onStop() {

                   if(!HourBroadcastReceiver.isActive) {
                       val intent = Intent(applicationContext, HourBroadcastReceiver::class.java)
                       val pendingIntent = PendingIntent.getBroadcast(applicationContext, 234324243, intent, 0)
                       val alarmManager =  getSystemService(ALARM_SERVICE) as AlarmManager
                       val calender = Calendar.getInstance()
                           calender.set(Calendar.MINUTE, calender.get(Calendar.MINUTE) + 1)
                           calender.set(Calendar.SECOND, 0)
                           calender.set(Calendar.MILLISECOND, 0)
                       alarmManager.set(AlarmManager.RTC, calender.timeInMillis, pendingIntent)
//
//                       if(isFirst) {
//                            /**
//                            * first run synchronising scheduler with system time
//                            * and on job execute creates new schedule with period*/
//                           //  isFirst = false
//                            val minutesUntilFull =  Calendar.getInstance().get(Calendar.SECOND)
//
//                            JobManager.instance().cancelAll()
//                            HourJob.scheduleJob(60 * 1000, minutesUntilFull * 1000)
//                        } else {
//                            HourJob.scheduleJob(60 * 1000, 0)
//                            //TODO replace scheduleJob with schedulePeriodicJob
//                          //HourJob.schedulePeriodicJob(60 * 1000L, 0)
//                        }
//                        HourTimeScheduler.isActive = true
                    }
                }
            }
            handleServiceStop.onStop()
            HourBroadcastReceiver.handleServiceStop = handleServiceStop
        return START_STICKY
    }
}
