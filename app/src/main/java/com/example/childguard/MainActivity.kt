package com.example.childguard

import android.app.ActivityManager
import android.app.admin.DeviceAdminReceiver
import android.app.admin.DevicePolicyManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import java.util.*
import androidx.fragment.app.FragmentActivity


class MainActivity : FragmentActivity() {

    companion object {
        const val HOLD_IN_APP = "HOLD_IN_APP"
        var isRunning : Boolean = false
        var finalizer : Finalizer? = null
    }

    interface Finalizer {
        fun finish()
    }
    lateinit var timer: Timer
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.addFlags(
    WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD or
    WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED or
                    WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON
        )

        isRunning = true
        val extra = intent.getBooleanExtra(HOLD_IN_APP, false)

        (application as App).inForegroud = true
        if (extra) { (applicationContext as App).holdInApp = extra
        }

        if(Calendar.getInstance().get(Calendar.MINUTE) % 2 == 0 && !(application as App).holdInApp) {
            (application as App).holdInApp =  true
        }


        finalizer = object : Finalizer {
            override fun finish() {
                (applicationContext as App).holdInApp = false
                isRunning = false
                try {
                    this@MainActivity.finish()
                    timer.stop()
                } catch (e : Exception) {
                    e.printStackTrace()
                }
            }
        }

        if(!FService.isRunning) {
            startForegroundService(Intent(this, FService::class.java))
        }
        setContentView(R.layout.activity_main)
        timer = findViewById(R.id.timer)
        }

    override fun onResume() {
        super.onResume()
        if ((applicationContext as App).holdInApp)
            Timer.start( timer,
                1 * 60 * 1000 - (Calendar.getInstance().get(Calendar.SECOND) * 1000L),
                    Locale.getDefault(), object  : Timer.TimeOut {
                        override fun onTimeOut() {
                            (application as App).holdInApp = false
                            timer.stop()
                            finish()
                        }
                    }
            )
    }

    override fun onBackPressed() {
        if(!(applicationContext as App).holdInApp) super.onBackPressed()
    }

    override fun onPause() {
        super.onPause()
        isRunning  = false
        if((applicationContext as App).holdInApp) {
            (application as App).inForegroud = false
            val activityManager =  applicationContext
                .getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            activityManager.moveTaskToFront(taskId,  ActivityManager.MOVE_TASK_WITH_HOME)
        }
    }
}
