package com.example.childguard

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import java.util.*


class HourBroadcastReceiver : BroadcastReceiver() {
    interface HandleServiceStop {
        fun onStop()
    }

    companion object {
        var isActive  = false
        var handleServiceStop : HandleServiceStop? = null
    }
    override fun onReceive(context: Context?, intent: Intent?) {
        if(Calendar.getInstance().get(Calendar.MINUTE) % 2 == 0) {
            Log.i("Job", "start")
            context?.startActivity(Intent(context, MainActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                addCategory("android.intent.category.LAUNCHER")
                putExtra(MainActivity.HOLD_IN_APP, true)
            })
        } else {
            Log.i("Job", "stop")
            MainActivity.finalizer?.finish()
        }
        if(!isActive)isActive = true
            isActive = false
            handleServiceStop?.onStop()
    }

}