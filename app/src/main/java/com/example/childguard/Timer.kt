package com.example.childguard

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import android.widget.TextView
import org.w3c.dom.Text
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

private var isRunning = false
class Timer @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : TextView(context, attrs, defStyleAttr) {


    fun stop() {
        isRunning = false
    }

    interface TimeOut {
        fun onTimeOut()
    }
   companion object {
       private var timer  : Timer? = null
       fun start(text : Timer, time : Long? = null, textLocale : Locale? = null, onTimeOnt : TimeOut) {
           timer = text
           if (!isRunning) {
               isRunning = true
               time?.let {
                   Handler().post {
                       val dateFormat = SimpleDateFormat("HH:mm:ss", textLocale)
                       var j = 0L
                       val cal = Calendar.getInstance()
                       val seconds = TimeUnit.MILLISECONDS.toSeconds(time)
                       cal.set(Calendar.HOUR_OF_DAY, 0)
                       cal.set(Calendar.MINUTE, 0)
                       cal.set(Calendar.SECOND, (seconds).toInt())
                       var startTime = cal.timeInMillis
                       for (i in seconds downTo 0L) {
                           Handler().postDelayed({
                               if (isRunning) {
                                   startTime -= 1000
                                   timer?.text = dateFormat.format(Date(startTime))
                                   println(dateFormat.format(Date(startTime)))
                                   if (i == 2L) {
                                       isRunning = false
                                       onTimeOnt.onTimeOut()
                                   } else {
                                       isRunning = true
                                   }
                               } else return@postDelayed
                           }, 1000 * j++)
                       }
               }
           }
       }
   }
    }
}